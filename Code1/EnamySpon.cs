﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnamySpon : MonoBehaviour
{
    public GameObject monster;
    public static EnamySpon instance;

    public Queue<GameObject> m_queue = new Queue<GameObject>();

    public float xPos;
    public float zPos;
    public int MaxQueue;
    private Vector3 RandomVector;


    // Start is called before the first frame update
    void Start()
    {
        MaxQueue = 10;
           instance = this;

        for (int i = 0; i < MaxQueue; i++)
        {
            GameObject t_object = Instantiate(monster, this.gameObject.transform);
            m_queue.Enqueue(t_object);
            t_object.SetActive(false);
        }

        StartCoroutine(MonsterSpawn());

    }

    IEnumerator MonsterSpawn()
    {
        while (true)
        {
            int m_queue_Count = m_queue.Count;
            if (m_queue_Count != 0)
            {
                for (int i = 0; i < m_queue_Count; i++)
                {
                    xPos = Random.Range(-5, 5);
                    zPos = Random.Range(-5, 5);
                    RandomVector = new Vector3(xPos, 0.0f, zPos);
                    GameObject t_object = GetQueue();
                    t_object.transform.position = gameObject.transform.position + RandomVector;
                }
                
            }
            yield return new WaitForSeconds(1f);
        }
    }

    public GameObject GetQueue()
    {
        GameObject t_object = m_queue.Dequeue();

        if (t_object == null)
        {
            t_object = Instantiate(monster, this.gameObject.transform);
        }
        t_object.SetActive(true);

        return t_object;
    }


    public void InsertQueue(GameObject p_object)
    {
        if(m_queue.Count > MaxQueue)
        {
            Destroy(p_object);
        }
        else
        {
            m_queue.Enqueue(p_object);
            p_object.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
