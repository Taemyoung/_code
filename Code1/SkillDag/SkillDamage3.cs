﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDamage3 : MonoBehaviour
{
    GameManager Instance;
    // Eff 19 에 적용 내용임 ㅇㅇ
    float Player_Skill3_Damege;

    public SphereCollider Hitbox;
    SkillPull1 eff;

    private void Awake()
    {
        Instance = GameManager.Instance;
        eff = SkillPull1.instance;

    }

    void Start()
    {
        Hitbox = gameObject.GetComponent<SphereCollider>();
        StartCoroutine(Damege_skill());

    }

    IEnumerator Damege_skill()
    {
        int count = 8;

        for (int a = 0; a < count; ++a)
        {
            Player_Skill3_Damege = 3.0f / Instance.PlayerDamage * 100;
            Hitbox.enabled = true;
            // Debug.LogError($"{Player_Skill3_Damege} PlayerDamege");
            yield return new WaitForSeconds(0.2f);
            Hitbox.enabled = false;
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(4.0f);
        eff.InsertQueue(gameObject,2);
        yield return null;
    }

    void FixedUpdate()
    {
        Instance.Skill3amegeval = Player_Skill3_Damege;
    }
}
