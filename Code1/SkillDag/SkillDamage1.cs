﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDamage1 : MonoBehaviour
{
    GameManager Instance ;
    // Eff 3 에 적용 내용임 ㅇㅇ
    float Player_Skill1_Damege;

    public SphereCollider Hitbox;

    SkillPull1 eff;

    private void Awake()
    {
        Instance = GameManager.Instance;
        eff = SkillPull1.instance;

    }
    void Start()
    {
        
        Hitbox = gameObject.GetComponent<SphereCollider>();
        StartCoroutine(Damege_skill());
        
    }

    IEnumerator Damege_skill()
    {
        int count = 10;
        
        for (int a = 0; a < count; ++a)
        {
            Player_Skill1_Damege =  1.7f / Instance.PlayerDamage * 100 ;
            Hitbox.enabled = true;
            // Debug.LogError($"{Player_Skill1_Damege} PlayerDamege");
            yield return new WaitForSeconds(0.1f);
            Hitbox.enabled = false;
            yield return new WaitForSeconds(0.4f);
        }

        Hitbox.enabled = true;
        yield return new WaitForSeconds(0.5f);
        Player_Skill1_Damege = 1.9f / Instance.PlayerDamage * 100 ;
        // Debug.LogError($"{Player_Skill1_Damege} PlayerDamege");
        Hitbox.enabled = false;
        yield return new WaitForSeconds(2.0f);
        eff.InsertQueue(gameObject, 0);
        yield return null;
    }
    
    void FixedUpdate()
    {
        Instance.Skill1amegeval = Player_Skill1_Damege;
    }
}
