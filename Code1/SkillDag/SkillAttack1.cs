﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillAttack1 : MonoBehaviour
{
    GameManager Instance;
    public BoxCollider Hitbox;

    float Player_attack_Damage;
    // attack Effect1 에 적용될 내용

    void Start()
    {
        Instance = GameManager.Instance;
        Hitbox = gameObject.GetComponent<BoxCollider>();
        StartCoroutine(Damage_Attack1());
    }

    IEnumerator Damage_Attack1()
    {
        Player_attack_Damage = 0.3f / Instance.PlayerDamage * 100;
        Hitbox.enabled = true;
        // Debug.LogError($"{Player_Skill1_Damege} PlayerDamege");
        yield return new WaitForSeconds(0.1f);
        Hitbox.enabled = false;
        
        yield return null;
    }
    // Update is called once per frame
    void Update()
    {
        Instance.Skill1amegeAttack = Player_attack_Damage;
    }
}
