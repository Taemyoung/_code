﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDamage2 : MonoBehaviour
{
    GameManager Instance ;
    // Eff 17 에 적용 내용임 ㅇㅇ
    float Player_Skill2_Damege;

    public BoxCollider Hitbox;

    SkillPull1 eff;

    private void Awake()
    {
        Instance = GameManager.Instance;
        eff = SkillPull1.instance;

    }

    void Start()
    {
        Hitbox = gameObject.GetComponent<BoxCollider>();
        StartCoroutine(Damege_skill());
        
    }

    IEnumerator Damege_skill()
    {

        Player_Skill2_Damege =  0.9f / Instance.PlayerDamage * 100 ;
        Hitbox.enabled = true;
        // Debug.LogError($"{Player_Skill2_Damege} PlayerDamege");
        yield return new WaitForSeconds(0.2f);
        Hitbox.enabled = false;
        yield return new WaitForSeconds(1.0f);


        Hitbox.enabled = true;
        Player_Skill2_Damege =  Instance.PlayerDamage * 2;
        yield return new WaitForSeconds(0.2f);
        // Debug.LogError($"{Player_Skill2_Damege} PlayerDamege");
        Hitbox.enabled = false;

        yield return new WaitForSeconds(3.0f);
        eff.InsertQueue(gameObject, 1);
        yield return null;
    }
    
    void FixedUpdate()
    {
        Instance.Skill2amegeval = Player_Skill2_Damege;
    }
}
