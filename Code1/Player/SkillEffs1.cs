﻿using UnityEngine;
using System.Collections;

public class SkillEffs1 : MonoBehaviour
{
    public int curnum;
    // public static SkillEffs1 skill_instance;
    public SkillPool SkillPool;

    public EffectInfo[] Effects;
    [System.Serializable]
    public class EffectInfo
    {
        public GameObject CharacterEffect;
        public Transform CharacterAttachPoint;
        public float CharacterEffect_DestroyTime = 10;
        [Space]

        public GameObject CharacterEffect2;
        public Transform CharacterAttachPoint2;
        public float CharacterEffect2_DestroyTime = 10;
        [Space]

        public GameObject MainEffect;
        public Transform AttachPoint;
        public Transform OverrideAttachPointToTarget;
        public float Effect_DestroyTime = 10;
        [Space]

        public GameObject AdditionalEffect;
        public Transform AdditionalEffectAttachPoint;
        public float AdditionalEffect_DestroyTime = 10;

        [HideInInspector] public bool IsMobile;

    }
    public void Awake()
    {
        SkillPool = GetComponent<SkillPool>();
    }
    public void Update()
    {
        
    }
    
    public void ActivateEffect(int EffectNumber)
    {
        curnum = EffectNumber;
        // if (Effects[EffectNumber].MainEffect == null) return;
        GameObject instance;
        instance = SkillPool.GetObject(EffectNumber);
        Debug.Log(instance);
        // if (Effects[EffectNumber].OverrideAttachPointToTarget == null) instance = Instantiate(Effects[EffectNumber].MainEffect, Effects[EffectNumber].AttachPoint.transform.position, Effects[EffectNumber].AttachPoint.transform.rotation);
        instance.transform.position = Effects[EffectNumber].AttachPoint.transform.position;
        instance.transform.rotation = Quaternion.LookRotation(-(Effects[EffectNumber].AttachPoint.position -
                                    Effects[EffectNumber].OverrideAttachPointToTarget.position));
        UpdateEffectForMobileIsNeed(instance);
        if (Effects[EffectNumber].Effect_DestroyTime > 0.01f)
            SkillPool.ReturnObject(instance, EffectNumber);
    }

    public void ActivateAdditionalEffect(int EffectNumber)
    {
        if (Effects[EffectNumber].AdditionalEffect == null) return;
        if (Effects[EffectNumber].AdditionalEffectAttachPoint != null)
        {
            var instance = Instantiate(Effects[EffectNumber].AdditionalEffect, Effects[EffectNumber].AdditionalEffectAttachPoint.transform.position, Effects[EffectNumber].AdditionalEffectAttachPoint.transform.rotation);
            UpdateEffectForMobileIsNeed(instance);
            if (Effects[EffectNumber].AdditionalEffect_DestroyTime > 0.01f) Destroy(instance, Effects[EffectNumber].AdditionalEffect_DestroyTime);
        }
        else Effects[EffectNumber].AdditionalEffect.SetActive(true);
    }

    public void ActivateCharacterEffect(int EffectNumber)
    {
        if (Effects[EffectNumber].CharacterEffect == null) return;
        var instance = Instantiate(Effects[EffectNumber].CharacterEffect,
                                    Effects[EffectNumber].CharacterAttachPoint.transform.position,
                                    Effects[EffectNumber].CharacterAttachPoint.transform.rotation,
                                    Effects[EffectNumber].CharacterAttachPoint.transform);
        UpdateEffectForMobileIsNeed(instance);
        if (Effects[EffectNumber].CharacterEffect_DestroyTime > 0.01f) Destroy(instance, Effects[EffectNumber].CharacterEffect_DestroyTime);
    }

    public void ActivateCharacterEffect2(int EffectNumber)
    {
        if (Effects[EffectNumber].CharacterEffect2 == null) return;
        var instance = Instantiate(Effects[EffectNumber].CharacterEffect2, Effects[EffectNumber].CharacterAttachPoint2.transform.position, Effects[EffectNumber].CharacterAttachPoint2.transform.rotation, Effects[EffectNumber].CharacterAttachPoint2);
        UpdateEffectForMobileIsNeed(instance);
        if (Effects[EffectNumber].CharacterEffect2_DestroyTime > 0.01f) Destroy(instance, Effects[EffectNumber].CharacterEffect2_DestroyTime);
    }

    void UpdateEffectForMobileIsNeed(GameObject instance)
    {
        // if (IsMobile)
        {
            var effectSettings = instance.GetComponent<RFX4_EffectSettings>();
            if (effectSettings != null)
            {
                //effectSettings.EffectQuality = IsMobile ? RFX4_EffectSettings.Quality.Mobile : RFX4_EffectSettings.Quality.PC;
                //effectSettings.ForceInitialize();
            }
        }
    }

}
