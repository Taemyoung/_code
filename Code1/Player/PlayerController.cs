﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    public GameObject cursor;
    GameManager Instance;
    public SkillPull1 skillEff;

    public float Max_Health;
    public float Cur_Health;
    public float Damage;

    public string Cur_Abnormal;

    public float StateTime ;

    public NavMeshAgent agent;
    public Camera playerCamera;
    public Vector3 targetpos;
    public bool isMove;
    public Transform PlayerTrans;

    public float speed = 7.0f;
    private Rigidbody rigid;
    public Animator anima;

    public Transform skill2_pos;
    private GameObject skill1_1;



    public int skill1_Dag;

    public enum PlayerState
    {
        idle,
        attack,
        skill1,
        skill2,
        skill3,
        dodge,
        Abnormal,
        die,

    }

    private Player_state<PlayerController> stateMachine;
    private Dictionary<PlayerState, IState<PlayerController>> dicstate = new Dictionary<PlayerState, IState<PlayerController>>();

    void Start()
    {
        Cur_Abnormal = "normal";
        Max_Health = 100f;
        Cur_Health = Max_Health;
        StateTime = 0f;
        Damage = 50f;

        skillEff = SkillPull1.instance;
        Instance = GameManager.Instance;
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        // agent.enabled = true;
        anima = GetComponent<Animator>();
        playerCamera = Camera.main;
        rigid = GetComponent<Rigidbody>();
        
        // 상태 생성
        IState<PlayerController> idle = new PlayerIdle();
        IState<PlayerController> attack = new PlayerAttack();
        IState<PlayerController> skill1 = new PlayerSkill1();
        IState<PlayerController> skill2 = new PlayerSkill2();
        IState<PlayerController> skill3 = new PlayerSkill3();
        IState<PlayerController> dodge = new PlayerDodge();
        IState<PlayerController> Abnormal = new PlayerAbnormal();
        IState<PlayerController> die = new PlayerDie();

        // 상태 보관
        dicstate.Add(PlayerState.idle, idle);
        dicstate.Add(PlayerState.attack, attack);
        dicstate.Add(PlayerState.skill1, skill1);
        dicstate.Add(PlayerState.skill2, skill2);
        dicstate.Add(PlayerState.skill3, skill3);
        dicstate.Add(PlayerState.dodge, dodge);
        dicstate.Add(PlayerState.Abnormal, Abnormal);
        dicstate.Add(PlayerState.die, die);

        stateMachine = new Player_state<PlayerController>(this, dicstate[PlayerState.idle]);
    }

    // Update is called once per frame
    void Update()
    {
        if (Cur_Health < 0)
        {
            ChangeState(PlayerState.die);
        }
        PlayerTrans = transform;

        stateMachine.DoOperateUpdate();
        dodgeCk();
    }

    // 회피
    public void dodgeCk()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            anima.SetTrigger("IsDodge");
            ChangeState(PlayerState.dodge);
        }
    }

    // State 교체
    public void ChangeState(PlayerState state)
    {
        stateMachine.SetState(dicstate[state]);
    }

    public void SetDestination2(Vector3 dest)
    {
        anima.SetBool("IsMove", true);
        targetpos = dest;
        isMove = true;
        // agent.SetDestination(dest);
    }

    public void Look()
    {
        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Plane GroupPlane = new Plane(Vector3.up, Vector3.zero);

        float rayLength;
        if (GroupPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointTolook = cameraRay.GetPoint(rayLength);
            transform.LookAt(new Vector3(pointTolook.x, transform.position.y, pointTolook.z));
        }
    }


    public void LookMove()
    {
        if (Input.GetMouseButton(1))
        {
            int _layerMask = 1 << LayerMask.NameToLayer("Map");
            RaycastHit hit;
            if(Physics.Raycast(playerCamera.ScreenPointToRay(Input.mousePosition), out hit,100f, _layerMask))
            {
                Transform TrnasCursor = hit.transform;
                Vector3 VectorCursor = hit.point;
                TrnasCursor.position = VectorCursor;

                // cursor 생성 '--------------------------- 작업 해야함
                // Instantiate(cursor, TrnasCursor);
                SetDestination2(hit.point);
            }
        }

        if (isMove)
        {
            var distance = targetpos - transform.position;
            var magnitude = distance.magnitude;
            // Debug.Log(magnitude);
            if (magnitude <= 0.3f)
            {
                anima.SetBool("IsMove", false);
                isMove = false;
            }

            var dir = distance.normalized;
            transform.position += dir * Time.deltaTime * speed;
            anima.transform.forward = dir;

            /*
            
            var distance = agent.velocity.magnitude;
            if (distance <= 0.3f)
            {
                ismove = false;
                anima.SetBool("IsMove", false);
                return;
            }

            var dir = new vector3 (agent.steeringtarget.x - transform.position.x ,
                                    transform.position.y,
                                    agent.steeringtarget.z
                                    );
            anima.transform.forward = dir;
            
            */
            // transform.position += dir.normalized * Time.deltaTime * speed;

        }
    }

    // 피격 처리 예정22222222222222222222222222222222222222222222
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == LayerMask.NameToLayer("EnamyAttack"))
        {
            
            Cur_Health -= Instance.EnamyDamage;
            // Debug.Log($"EnamyAttack{Cur_Health}");
        }

        if (other.gameObject.layer == LayerMask.NameToLayer("EnamyDownAttack"))
        {
            Cur_Abnormal = "Down";
            Cur_Health -= 2;
            // Debug.Log($"EnamyDownAttack{Cur_Health}");
            ChangeState(PlayerState.Abnormal);

        }
        else if (other.gameObject.layer == LayerMask.NameToLayer("EnamyPushAttack"))
        {
            Cur_Abnormal = "Push";
            Cur_Health -= 3; 
            // Debug.Log($"EnamyPushAttack{Cur_Health}");
            ChangeState(PlayerState.Abnormal);
        }
        
    }


    // Trigger 문제 수정 예정
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Enamy"))
        {
            // Debug.LogError("s");
            isMove = false;
            transform.position = transform.position;
        }
    }
}
