﻿using UnityEngine;

public class Player_state<T>
{
    // 현재상태를 담는 
    public IState<T> CurrentState { get; protected set; }

    private T m_sender;

    // 생성자 
    public Player_state(T sender, IState<T> defaultState)
    {
        m_sender = sender;
        SetState(defaultState);
        
    }

    // 현재 상태 변경
    public void SetState(IState<T> state)
    {
        Debug.LogWarning("SetState : " + state); // 로그출력

        // null에러출력
        if (m_sender == null)
        {
            return;
        }

        // 같은 처리 못하게 막는 코드
        if (CurrentState == state)
        {
            return;
        }

        // 상태 변경 하기위해 이전 루틴 종료
        if (CurrentState != null)
            CurrentState.OperateExit(m_sender);

        //상태 교체 
        CurrentState = state;

        // 새 상태의 Enter 를 호출
        if (CurrentState != null)
            CurrentState.OperateEnter(m_sender);


    }

    public void DoOperateUpdate()
    {
        if (m_sender == null)
        {
            // null에러 메시지
            return;
        }

        // update 
        CurrentState.OperateUpdate(m_sender);
    }

}
