﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPool : MonoBehaviour
{
    public static SkillPool Instance;

    public EffectPool[] Effectpool;

    [System.Serializable]
    public class EffectPool
    {
        public GameObject poolingObjectPrefab;
        public int Count;
        public Queue<GameObject> poolingObjectQueue = new Queue<GameObject>();

    }

    public void Awake()
    {
        Instance = this;
        for (int EffpoolCnt = 0; EffpoolCnt < Effectpool.Length; EffpoolCnt++)
        {
            Debug.Log(EffpoolCnt);
            Initialize(Effectpool[EffpoolCnt].Count, EffpoolCnt);
        }
    }
    
    public void Initialize(int initCount,int EffectNumber)
    {
            for (int i = 0; i < initCount; i++)
            {
                Effectpool[EffectNumber].poolingObjectQueue.Enqueue(CreateNewObject( EffectNumber));
            }
    }

    // 생성
    public GameObject CreateNewObject(int arrCount)
    {
        var newObj = Instantiate(Effectpool[arrCount].poolingObjectPrefab);
        newObj.gameObject.SetActive(false);
        newObj.transform.SetParent(transform);
        return newObj;
    }


    // 오브젝트 주기
    public static GameObject GetObject(int EffectNumber)
    {
        if (Instance.Effectpool[EffectNumber].poolingObjectQueue.Count > 0)
        {
            var obj = Instance.Effectpool[EffectNumber].poolingObjectQueue.Dequeue();
            obj.transform.SetParent(null);
            obj.gameObject.SetActive(true);
            return obj;
        }
        else
        {
            var newObj = Instance.CreateNewObject(EffectNumber);
            newObj.gameObject.SetActive(true);
            newObj.transform.SetParent(null);
            return newObj;
        }
    }

    // 오브젝트 돌려받기
    public static void ReturnObject(GameObject obj, int EffectNumber)
    {
        obj.gameObject.SetActive(false);
        obj.transform.SetParent(Instance.transform);
        Instance.Effectpool[EffectNumber].poolingObjectQueue.Enqueue(obj);
    }




}
