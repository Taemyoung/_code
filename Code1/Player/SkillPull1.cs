﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPull1 : MonoBehaviour
{

    public SkillObject[] skillobjects;

    [System.Serializable]
    public class SkillObject
    {
        public GameObject SkillEff;
        public Queue<GameObject> m_queue = new Queue<GameObject>();
    }
    
    public static SkillPull1 instance;

    public bool isValidated
    {
        get
        {
            return (skillobjects != null && skillobjects.Length > 0);
        }
    }


    public int MaxQueue;

    void Start()
    {
           MaxQueue = 2;
           instance = this;
           int a = 0;

        for (int i = 0; i < skillobjects.Length; i++)
        {
            Initialize(MaxQueue, i);
            // GameObject t_object = Instantiate(skillobjects[i].SkillEff, this.gameObject.transform);
            
        }


    }
    public void Initialize(int initCount, int EffectNumber)
    {
        for (int i = 0; i < initCount; i++)
        {
            skillobjects[EffectNumber].m_queue.Enqueue(CreateNewObject(EffectNumber));
        }
    }
    public GameObject CreateNewObject(int arrCount)
    {
        GameObject newObj = Instantiate(skillobjects[arrCount].SkillEff, this.gameObject.transform);
        newObj.gameObject.SetActive(false);
        // newObj.transform.SetParent(transform);
        return newObj;
    }

    public GameObject GetQueue22(int z)
    {
        if(skillobjects[z].m_queue.Count == 0)
        {
            GameObject newObj = Instantiate(skillobjects[z].SkillEff, this.gameObject.transform);
            newObj.SetActive(true);
            return newObj;
        }
        else
        {
            GameObject t_object = skillobjects[z].m_queue.Dequeue();
            t_object.SetActive(true);
            return t_object;
        }

    }

    public void InsertQueue(GameObject p_object, int z)
    {
        if(skillobjects[z].m_queue.Count > MaxQueue)
        {
            Destroy(gameObject);
        }
        else
        {
            // Debug.Log(skillobjects[z].m_queue.Count);
            skillobjects[z].m_queue.Enqueue(p_object);
            p_object.SetActive(false);

        }
    }
}