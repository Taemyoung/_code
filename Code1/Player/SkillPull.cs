﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillPull : MonoBehaviour
{
    public GameObject SkillEff;
    public static SkillPull instance;

    public Queue<GameObject> m_queue = new Queue<GameObject>();

    public int MaxQueue;


    // Start is called before the first frame update
    void Start()
    {
           MaxQueue = 20;
           instance = this;

        for (int i = 0; i < MaxQueue; i++)
        {
            GameObject t_object = Instantiate(SkillEff, this.gameObject.transform);
            m_queue.Enqueue(t_object);
            t_object.SetActive(false);
        }


    }

    public GameObject GetQueue()
    {

        GameObject t_object = m_queue.Dequeue();
        t_object.SetActive(true);

        return t_object;
    }


    public void InsertQueue(GameObject p_object)
    {
            m_queue.Enqueue(p_object);
            p_object.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

}