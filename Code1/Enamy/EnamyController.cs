﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnamyController : MonoBehaviour
{
    bool Respon;
    [Header("Boss Change Cnt")]
    public SkillPull AttackEff;
    public Transform AttackPos;
    GameManager Instance;

    public float Damage;
    float MaxHp;
    public float CurHp;

    public float StateTime;
    public Transform Target;
    public NavMeshAgent nav;
    public Animator anima;

    public enum EnamyState
    {
        idle,
        attack,
        Abnormal,
        Die,

    }

    private Player_state<EnamyController> stateMachine;
    private Dictionary<EnamyState, IState<EnamyController>> dicstate = new Dictionary<EnamyState, IState<EnamyController>>();


    // Start is called before the first frame update
    void Start()

    {
        Damage = 0.5f;
        MaxHp = 100;
        CurHp = MaxHp;
        AttackEff = SkillPull.instance;
        nav = GetComponent<NavMeshAgent>();
        Instance = GameManager.Instance;
        Target = GameObject.Find("Player").transform;
        anima = GetComponent<Animator>();

        IState<EnamyController> enamyidle = new EnamyIdle();
        IState<EnamyController> enamyattack = new EnamyAttack();
        IState<EnamyController> enamydie = new EnamyDie();


        dicstate.Add(EnamyState.idle, enamyidle);
        dicstate.Add(EnamyState.attack, enamyattack);
        dicstate.Add(EnamyState.Die, enamydie);
        Respon = false;

        stateMachine = new Player_state<EnamyController>(this, dicstate[EnamyState.idle]);
        Respon = true;

    }

    private void OnEnable() 
    {
        CurHp = (int)MaxHp;
        if(Respon)
            stateMachine = new Player_state<EnamyController>(this, dicstate[EnamyState.idle]);
    }

    public void Die()
    {
        Invoke("Die2", 4f);
    }
    void Die2()
    {
        gameObject.SetActive(false);
        EnamySpon.instance.InsertQueue(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == LayerMask.NameToLayer("PlayerAttack"))
        {
            // StartCoroutine(UnBeatTime());
            // Debug.LogError($"Trigger LayerMask Hit {other.gameObject.name}");
            // 피격처리 필요
            var a = other.name;
            if (a == "Effect19(Clone)")
            {
                CurHp -= Instance.Skill3amegeval;
                // Debug.LogError($"{a} {CurHp}");
            }
            if (a == "Effect17_Collision(Clone)")
            {
                CurHp -= Instance.Skill2amegeval;
                // Debug.LogError($"{a} {CurHp}");
            }
            if (a == "Effect3(Clone)")
            {
                CurHp -= Instance.Skill1amegeval;
                // Debug.LogError($"{a} {CurHp}");
            }
            if (a == "Effect1_Collision(Clone)")
            {
                CurHp -= Instance.Skill1amegeAttack;
                // Debug.LogError($"{a} {CurHp}");
            }

        }
    }
    IEnumerator UnBeatTime()
    {
        // EnamyMaterial.color = new Color32(255, 0, 0, 255);
        yield return new WaitForSeconds(0.9f);

          // EnamyMaterial.color = new Color32(255, 255, 255, 255);

    }
    // State 교체
    public void ChangeState(EnamyState state)
    {
        stateMachine.SetState(dicstate[state]);
    }

    void Update()
    {


        Instance.EnamyDamage = Damage;
        stateMachine.DoOperateUpdate();
        if (CurHp < 0)
        {
            ChangeState(EnamyState.Die);

        }

    }
}
