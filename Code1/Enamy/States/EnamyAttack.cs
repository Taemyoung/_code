﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnamyAttack : IState<EnamyController>
{
    GameObject a;

    public void OperateEnter(EnamyController Enamy)
    {
        Enamy.StateTime = 0f;
        Enamy.anima.SetTrigger("IsAttack");
        Enamy.gameObject.transform.LookAt(Enamy.Target.position);
        a = Enamy.AttackEff.GetQueue();
        a.transform.position = Enamy.AttackPos.position;
        a.transform.rotation = Enamy.AttackPos.rotation;

    }

    public void OperateExit(EnamyController Enamy)
    {

    }

    public void OperateUpdate(EnamyController Enamy)
    {
        Enamy.StateTime += Time.deltaTime;




        if (Enamy.StateTime > 4f)
        {
            // 공격 처리
            Enamy.ChangeState(EnamyController.EnamyState.idle);
        }
        else
        {
            // Debug.Log("공격중");
        }

    }

}
