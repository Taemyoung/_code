﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnamyDie : IState<EnamyController>
{ 
    public void OperateEnter(EnamyController Enamy)
    {
        Enamy.Die();
        Enamy.anima.SetTrigger("IsDie");

    }

    public void OperateExit(EnamyController Enamy)
    {
        Enamy.ChangeState(EnamyController.EnamyState.idle);
    }

    public void OperateUpdate(EnamyController Enamy)
    {

    }


}

