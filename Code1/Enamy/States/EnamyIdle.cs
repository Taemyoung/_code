﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnamyIdle : IState<EnamyController>
{
    public void OperateEnter(EnamyController Enamy)
    {
        Enamy.nav.isStopped = false;
    }

    public void OperateExit(EnamyController Enamy)
    {
        Enamy.anima.SetBool("IsMove", false);
        Enamy.nav.isStopped = true;
        Enamy.nav.velocity = Vector3.zero;
    }

    public void OperateUpdate(EnamyController Enamy)
    {
        Vector3 myPos = Enamy.gameObject.transform.position;
        Enamy.gameObject.transform.position = new Vector3 (Enamy.transform.position.x, 0.04f, Enamy.transform.position.z);

        Vector3 TargetPos = Enamy.Target.position;
        TargetPos.y = 0;

        Vector3 Ma = TargetPos - myPos;
        float target_magnitude = Ma.magnitude;


        // Debug.Log(target_magnitude);

            if (target_magnitude < 4f)
            {
                // 공격으로 이동
                // Debug.Log("Attack chnage " + target_magnitude);
                Enamy.ChangeState(EnamyController.EnamyState.attack);
            }
            else
            {
                Enamy.anima.SetBool("IsMove", true);
                Enamy.nav.SetDestination(Enamy.Target.position);
            }   
     

    }

}
