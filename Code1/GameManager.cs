﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    
    public PlayerController player;
    public Transform PlayerTrans;

    public float Skill1amegeval;
    public float Skill2amegeval;
    public float Skill3amegeval;
    public float Skill1amegeAttack;
   
    public float PlayerDamage;
    public float EnamyDamage;

    public static GameManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(GameManager)) as GameManager;

                if (_instance == null)
                    Debug.Log("no Singleton obj");
            }
            return _instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        // 인스턴스가 존재하는 경우 새로생기는 인스턴스를 삭제한다.
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
        player = GameObject.Find("Player").GetComponent<PlayerController>();

        


    }


    // Update is called once per frame
    void FixedUpdate()
    {
        PlayerTrans = player.PlayerTrans;
        PlayerDamage = player.Damage;
        
    }
}
