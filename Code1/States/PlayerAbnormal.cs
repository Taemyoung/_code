﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAbnormal : IState<PlayerController>
{
    string Cur_Abnormal;
    float Abnormal_Time;
    bool UpTrigger;
    public void OperateEnter(PlayerController Player)
    {
        // 초기화 
        Player.StateTime = 0f;
        Cur_Abnormal = Player.Cur_Abnormal;
        UpTrigger = true;
        if (Cur_Abnormal == "Down")
        {
            Abnormal_Time = 2f;
            Player.anima.SetTrigger("IsDown");

        }
        else if (Cur_Abnormal == "Push")
        {
            Abnormal_Time = 0.7f;
            Player.anima.SetTrigger("IsPush");
        }


        Player.anima.SetBool("IsMove", false);
    }

    public void OperateExit(PlayerController Player)
    {
        Player.isMove = false;
    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.StateTime += Time.deltaTime;
        if (Player.StateTime > Abnormal_Time)
        {
            Player.anima.SetTrigger("IsIdle");
            Player.ChangeState(PlayerController.PlayerState.idle);
        }
        else
        {
            if (Cur_Abnormal == "Down")
            {
                Player.StartCoroutine(OnDown(Player));
            }
            else if (Cur_Abnormal == "Push")
            {
                Player.StartCoroutine(OnPush(Player));
            }

        }
    }

    // 밀기 공격 당했을때
    IEnumerator OnPush(PlayerController Player)
    {
        if (Player.StateTime < 0.5f)
        {
            Player.transform.localPosition += -Player.transform.forward * 4f * Time.deltaTime;
        }

        yield return null;
    }

    // 다운 공격 당했을때
    IEnumerator OnDown(PlayerController Player)
    {
        if (Player.StateTime < 1.3f)
        {
            Player.transform.localPosition += -Player.transform.forward * 2f * Time.deltaTime;
        } else
        {
            if(UpTrigger)
                Player.anima.SetTrigger("IsArise");
            UpTrigger = false;
        }

        yield return null; 
    }
}
