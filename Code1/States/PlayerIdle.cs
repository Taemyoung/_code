﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdle : IState<PlayerController>
{
    
    public void OperateEnter(PlayerController Player)
    {
        Player.agent.isStopped = false;
        Player.StateTime = 0f;
    }

    public void OperateExit(PlayerController Player)
    {
        Player.agent.isStopped = true;
        Player.agent.velocity = Vector3.zero;
    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.LookMove();

        if (Input.GetMouseButton(0))
        {
            Player.anima.SetTrigger("IsAttack1");
            Player.ChangeState(PlayerController.PlayerState.attack);
        }
        if (Input.GetKey("1"))
        {
            Player.anima.SetTrigger("IsSkill1");
            Player.ChangeState(PlayerController.PlayerState.skill1);
        }
        if (Input.GetKey("2"))
        {
            Player.anima.SetTrigger("IsSkill2");
            Player.ChangeState(PlayerController.PlayerState.skill2);
        }
        if (Input.GetKey("3"))
        {
            Player.anima.SetTrigger("IsSkill3");
            Player.ChangeState(PlayerController.PlayerState.skill3);
        }
        

    }

}
