﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : IState<PlayerController>
{
    public void OperateEnter(PlayerController Player)
    {
        // 초기화 
        Player.StateTime = 0f;

        Player.Look();
        Player.anima.SetBool("IsMove", false);
    }

    public void OperateExit(PlayerController Player)
    {
        Player.isMove = false; 
    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.StateTime += Time.deltaTime;
        if (Player.StateTime > 1.0f)
        {
            Player.anima.SetTrigger("IsIdle");
            Player.ChangeState(PlayerController.PlayerState.idle);
        }
        else
        {

        }
    }

}
