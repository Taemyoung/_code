﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill3 : IState<PlayerController>
{
    public void OperateEnter(PlayerController Player)
    {
        Player.StateTime = 0f;
        Player.anima.SetBool("IsMove", false);

        // Player.Look();
        SkillPull1 eff = SkillPull1.instance;

        if (eff != null && eff.isValidated)
        {
            var z = eff.GetQueue22(2);
            z.transform.position = Player.transform.position;
            z.transform.rotation = Player.transform.rotation;
        }

    }

    public void OperateExit(PlayerController Player)
    {
        Player.isMove = false;

    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.StateTime += Time.deltaTime;
        if (Player.StateTime > 1.2f)
        {
            Player.anima.SetTrigger("IsIdle");
            Player.ChangeState(PlayerController.PlayerState.idle);
        }
        else
        {

        }
    }
}
