﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill1 : IState<PlayerController>
{
    Vector3 target;
    bool SkillCot;

    public void OperateEnter(PlayerController Player)
    {
        // 초기화 
        Player.StateTime = 0f;
        Player.anima.SetBool("IsMove", false);

        Player.Look();
        SkillCot = true;
        // 스킬 생성

        RaycastHit hit;
        if (Physics.Raycast(Player.playerCamera.ScreenPointToRay(Input.mousePosition), out hit))
            {
                target = hit.point;
            }


        // Player.skillEff.GetQueue(0);
        // Player.SkillCreate( target, Player.transform.rotation);

    }

    public void OperateExit(PlayerController Player)
    {
        Player.isMove = false;
    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.StateTime += Time.deltaTime;
        if (Player.StateTime > 1.3f)
        {
            Player.anima.SetTrigger("IsIdle");
            Player.ChangeState(PlayerController.PlayerState.idle);
        }
        else
        {

        }

        if (Player.StateTime > 1.0f && SkillCot)
        {
            SkillPull1 eff = SkillPull1.instance;

            if (eff != null && eff.isValidated)
            {
                var z = eff.GetQueue22(0);
                z.transform.position = target;
            }
            SkillCot = false;
        }
    }

}
