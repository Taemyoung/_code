﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill2 : IState<PlayerController>
{
    Vector3 target;
    bool SkillCot;

    public void OperateEnter(PlayerController Player)
    {
        Player.StateTime = 0f;
        Player.anima.SetBool("IsMove", false);

        Player.Look();

        SkillCot = true;
    }

    public void OperateExit(PlayerController Player)
    {
        Player.isMove = false;

    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.StateTime += Time.deltaTime;
        if (Player.StateTime > 1.2f)
        {
            Player.anima.SetTrigger("IsIdle");
            Player.ChangeState(PlayerController.PlayerState.idle);
        }
        else
        {

        }

        if (Player.StateTime > 0.8f && SkillCot)
        {
            SkillPull1 eff = SkillPull1.instance;

            if (eff != null && eff.isValidated)
            {
                var z = eff.GetQueue22(1);
                z.transform.position = Player.skill2_pos.position;
                z.transform.rotation = Player.skill2_pos.rotation;
            }
            SkillCot = false;
        }
        


    }

}
