﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDie : IState<PlayerController>
{
    public void OperateEnter(PlayerController Player)
    {
        Player.anima.SetTrigger("IsDie");
        Player.anima.SetBool("IsMove", false);
    }

    public void OperateExit(PlayerController Player)
    {

    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.StateTime += Time.deltaTime;
        if (Player.StateTime > 6f)
        {
            Debug.Log("Scene le");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else
        {

        }

    }

   
}
