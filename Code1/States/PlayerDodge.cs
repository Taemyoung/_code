﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDodge : IState<PlayerController>
{
    float Dogspeed = 4.0f;

    public void OperateEnter(PlayerController Player)
    {
        // Player.agent.isStopped = false;

        // Player.agent.SetDestination(-Player.transform.forward * 2);

        // 초기화 
        Player.StateTime = 0f;

        Player.Look();
        Player.anima.SetBool("IsMove", false);
        Dogspeed = 4.0f + Player.speed;
    }

    public void OperateExit(PlayerController Player)
    {
        Player.isMove = false;
    }

    public void OperateUpdate(PlayerController Player)
    {
        Player.StateTime += Time.deltaTime;
        if (Player.StateTime > 0.8f)
        {
            Player.anima.SetTrigger("IsIdle");
            Player.ChangeState(PlayerController.PlayerState.idle);
        }
        else
        {
            
            Player.transform.localPosition += -Player.transform.forward * Dogspeed * Time.deltaTime;
        }
    }

}
