﻿
public interface IState<T>
{
    // 상태 최상위 인터 페이스
    void OperateEnter(T sender);
    void OperateUpdate(T sender);
    void OperateExit(T sender);
}
