﻿using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public static Boss bos;
    public Skill_Eff boss_Eff;
    public Material bossmat;
    public Rigidbody boss_rigi;

    // 보스와 플레이어 위치
    [Header("Play and Boss Relation")]
    public float ViewAngle = 90; // 시야각
    public float sprLen = 0;
    public float player_LR = 0;
    public float player_FB = 0;
    public Vector3 offset = Vector3.zero;

    // 보스 패턴 Count
    [Header("Boss Change Cnt")]
    public float curtime = 0;
    public float delaytime = 2f;
    public float speed = 2f;

    // 보스 Hp
    [Header("Boss Hp and Grogi Cnt")]
    public int _bossHp;
    public int _MaxbossHp = 20;
    public int _grogicnt = 0;
    public int _hitcnt = 0;
    public bool _Nohitcek;

    [Header("Component")]
    public Transform _mytrans;
    public Transform _target;
    public GameObject myobj;
    public Animator animator;
    public BoxCollider boxcollier;
    public string statename = "walk";
    public int state_cnt = 1;

    [Header("Eff Component")]
    public Transform Attack_L;
    public Transform Attack_R;
    public Transform Attack_G;
    public Transform Attack_P1;

    public enum BossState
    {
        idle,
        move,
        attackLeft,
        attackRight,
        jump,
        pattern1,
        pattern2,
        die,
        grogi,
        turn
    }

    private Boss_state<Boss> stateMachine;

    // 스테이트들을 보관
    private Dictionary<BossState, IState<Boss>> dicstate = new Dictionary<BossState, IState<Boss>>();

    [System.Obsolete]
    void Start()
    {
        // player 위치 가져오기
        _target = GameObject.FindWithTag("Player").GetComponent<Transform>();
        boss_rigi = GetComponent<Rigidbody>();
        bos = GetComponent<Boss>();
        myobj = gameObject;
        _mytrans = bos.transform;
        _bossHp = _MaxbossHp;
        boxcollier = GetComponent<BoxCollider>();
        // boss_Eff = GameObject.Find("skilltest").GetComponent<Skill_Eff>();
        boss_Eff = Skill_Eff.skill_instance;

        Attack_P1 = GameObject.Find("MountainDragon_ Head").transform.GetChild(1);
        Attack_L = GameObject.Find("BossEff").transform.GetChild(0);
        Attack_R = GameObject.Find("BossEff").transform.GetChild(1);
        Attack_G = GameObject.Find("BossEff").transform.GetChild(3);
        _Nohitcek = false;

        // 상태 생성
        IState <Boss> idle = new BossStartIdle();
        IState<Boss> move = new BossStartmove();
        IState<Boss> attackLeft = new BossStartAttackLeft();
        IState<Boss> attackRight = new BossStartAttackRight();
        IState<Boss> jump = new BossStartJump();
        IState<Boss> pattern1 = new BossStartPattern1();
        IState<Boss> pattern2 = new BossStartPattern2();
        IState<Boss> die = new BossStartDie();
        IState<Boss> grogi = new BossStartGrogi();
        IState<Boss> turn = new BossStartTurn();

        // 상태 보관
        dicstate.Add(BossState.idle, idle);
        dicstate.Add(BossState.move, move);
        dicstate.Add(BossState.attackLeft, attackLeft);
        dicstate.Add(BossState.attackRight, attackRight);
        dicstate.Add(BossState.jump, jump);
        dicstate.Add(BossState.pattern1, pattern1);
        dicstate.Add(BossState.pattern2, pattern2);
        dicstate.Add(BossState.die, die);
        dicstate.Add(BossState.grogi, grogi);
        dicstate.Add(BossState.turn, turn);

        // 시작 상태를 idle로 생성
        stateMachine = new Boss_state<Boss>(this, dicstate[BossState.idle]);

    }

    private void Update()
    {
        
        Vector3 my_pos = _mytrans.position;
        Vector3 target_pos = _target.position;

        // 보스 양옆 체크
        player_LR = Vector3.Cross(my_pos, target_pos).y;
        // 보스 전방 체크
        player_FB = Vector3.Dot(transform.forward, (target_pos - my_pos).normalized);
        offset = _target.position - _mytrans.position;

        // 보스와 플레이어 거리 체크
        sprLen = offset.sqrMagnitude;
        curtime += Time.deltaTime;

        // 그로기 체크는 상시 발생
        BossGrogi();
        // 업데이트 
        stateMachine.DoOperateUpdate();

    }


    // State 교체
    public void ChangeState(BossState state)
    {
        stateMachine.SetState(dicstate[state]);
    }

    // 보스 이동 
    public void BossMove(float sprLen, float player_FB)
    {
        // 보스와 플레이어 사이거리
        // 보스 공격 범위밖에 플레이어 존재 따라가기
        if (sprLen > 150)
        {
            float Angle = 45f;

            // 보스 앞쪽에 적이 있을경우
            if (player_FB >= Mathf.Cos((Angle * Mathf.Deg2Rad) * 0.5f))
            {
                // 보스 플레이어 방향으로 달리기
                state_cnt = 5;
            }
            else
            {
                // 보스 플레이어 방향으로 돌기 
                state_cnt = 9;
            }

        }
        else if (sprLen < 150 && sprLen > 80)
        {
            //보스 플레이어 방향으로 걷기
            state_cnt = 2;
        }
    }

    public void BossAttack(float sprLen, float player_FB)
    {
        float Angle = 180f;
        if (sprLen <= 70f)
        {
            // 보스 앞쪽에 적이 있을경우
            if (player_FB >= Mathf.Cos((Angle * Mathf.Deg2Rad) * 0.5f))
            {
                int RandumInt = Random.Range(3, 5);
                state_cnt = RandumInt;
            }
            else
            {
                // 뒤로 가기
                state_cnt = 7;
            }
        }
     
    }

    // 그로기 체크 및 넘어가는 패턴
    public void BossGrogi()
    {
        if (_hitcnt >= 10)
        {
            _grogicnt++;
        }
        if (_grogicnt == 1)
        {
            // grogi
            state_cnt = 8;

            ChangeState(Boss.BossState.grogi); 
            _hitcnt = 0;
            _grogicnt = 0;
        }
    }
    
    // 피격 로직
    public void OnTriggerEnter(Collider other)
    {
        if (_Nohitcek)
        {
        }
        else if (other.tag == "PlayerAttack")
        {
            StartCoroutine(UnBeatTime());
            _hitcnt += 2;
             _bossHp -= 1;
        } 
    }
    // 피격시 색변환
    System.Collections.IEnumerator UnBeatTime()
    {
        _Nohitcek = true;
        bossmat.color = new Color32(255, 0, 0, 255);
        yield return new WaitForSeconds(0.9f);

        bossmat.color = new Color32(255, 255, 255, 255);

        _Nohitcek = false;
        yield return null;
    }

    // 보스와 player rotate 구하기
    public Quaternion PB_rotate(float Speed)
    {
        Vector3 dir = _target.position - transform.position;
        Quaternion pb_rotate = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * Speed);

        return pb_rotate;
    }

}
