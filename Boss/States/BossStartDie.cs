﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartDie : IState<Boss>
{
    public GameObject Boss = GameObject.Find("MountainDragon_PBR");
    public Animator animator;
    public void OperateEnter(Boss boss)
    {
        // 죽음 모션
        animator = Boss.GetComponent<Animator>();
        animator.SetBool("Die", true);
    }

    public void OperateExit(Boss boss)
    {
       
    }

    public void OperateUpdate(Boss boss)
    {
    }
}
