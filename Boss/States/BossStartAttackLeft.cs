﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartAttackLeft : IState<Boss>
{
    float speed = 2.0f;
    bool attackcnt;

    public void OperateEnter(Boss boss)
    {
        attackcnt = true;
        boss.curtime = 0;
        // 왼쪽 공격
        boss.statename = "AttackLeft"; 
        boss.animator.SetBool("AttackLeft", true);


    }
    public void OperateExit(Boss boss)
    {
        boss.animator.SetBool("AttackLeft", false);
    }

    public void OperateUpdate(Boss boss)
    {
        if (boss.curtime >= 1.5f)
        {
            // 초기화
            boss.curtime = 0;
            boss.ChangeState(Boss.BossState.idle);
        }
        else
        {
            // 동작할때 
            boss.transform.rotation = boss.PB_rotate(2f);

            if (boss.curtime >= 0.9f)
            {
                if (attackcnt)
                {
                    boss.boss_Eff.InstantiateEffect(0, boss.Attack_L, 2f);
                    attackcnt = false;
                }
                
            }

        }

    }
}
