﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartPattern1 : IState<Boss>
{
    float patten1time;
    float speed = 1;
    int attackcnt = 0;
    public void OperateEnter(Boss boss)
    {
        attackcnt = 0;
        patten1time = 0;
        boss.statename = "Patten1";
        boss.animator.SetBool("Patten1", true);
    }

    public void OperateExit(Boss boss)
    {
        boss.animator.SetBool("Patten1", false);
    }

    public void OperateUpdate(Boss boss)
    {
        patten1time += Time.deltaTime;

        if (patten1time >= 10f)
        {
            // 초기화
            boss.curtime = 0;
            boss.ChangeState(Boss.BossState.idle);
        }
        else
        {
            boss.curtime = 0;
            if (patten1time > 5f && patten1time < 7f)
            {
                boss.transform.rotation = boss.PB_rotate(1f);
                attackcnt++;
                if (attackcnt % 3 == 0)
                {
                    boss.boss_Eff.InstantiateEffect(4, boss.Attack_P1, 2f);

                }
            }
          
        }


    }
}

