﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartIdle : IState<Boss>
{
   public void OperateEnter(Boss boss)
    {
        boss.animator.SetBool("walk", false);
        boss.animator.SetBool("AttackLeft", false);
        boss.animator.SetBool("AttackRight", false);
        boss.animator.SetBool("Grogi", false);
        boss.animator.SetBool("Die", false);
        boss.animator.SetBool("Jump", false);
        boss.animator.SetBool("Patten1", false);
        boss.animator.SetBool("TurnRight", false);
        boss.animator.SetBool("TurnLeft", false);
    }

    public void OperateExit(Boss boss)
    {
       
    }

    public void OperateUpdate(Boss boss)
    {

        if (boss.curtime >= boss.delaytime)
        {
            // 초기화
            boss.curtime = 0;

            boss.BossMove(boss.sprLen, boss.player_FB);
            boss.BossAttack(boss.sprLen, boss.player_FB);
            
            statechange(boss);

        }

    }
    void statechange(Boss boss)
    {
        switch (boss.state_cnt)
        {
            case 1:
                boss.ChangeState(Boss.BossState.idle);
                break;
            case 2:
                boss.ChangeState(Boss.BossState.move);
                break;
            case 3:
                boss.ChangeState(Boss.BossState.attackLeft);
                break;
            case 4:
                boss.ChangeState(Boss.BossState.attackRight);
                break;
            case 5:
                boss.ChangeState(Boss.BossState.jump);
                break;
            case 6:
                boss.ChangeState(Boss.BossState.pattern1);
                break;
            case 7:
                boss.ChangeState(Boss.BossState.pattern2);
                break;
            case 8:
                boss.ChangeState(Boss.BossState.grogi);
                break;
            case 9:
                boss.ChangeState(Boss.BossState.turn);
                break;
            default:
                boss.ChangeState(Boss.BossState.idle);
                break;
        }
    }

}
