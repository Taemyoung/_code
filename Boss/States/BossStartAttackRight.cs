﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartAttackRight : IState<Boss>
{
    float speed = 2.0f;
    bool attackcnt;

    public void OperateEnter(Boss boss)
    {
        attackcnt = true;
        boss.curtime = 0;
        boss.statename = "AttackRight";
        boss.animator.SetBool("AttackRight", true);
        
    }

    public void OperateExit(Boss boss)
    {
        boss.animator.SetBool("AttackRight", false);
    }

    public void OperateUpdate(Boss boss)
    {
        if (boss.curtime >= 1.5f)
        {
            // 초기화
            boss.curtime = 0;
            boss.ChangeState(Boss.BossState.idle);
        }
        else
        {
            // 동작할때 처리
            boss.transform.rotation = boss.PB_rotate(2f);
            if (boss.curtime >= 0.9f)
            {
                // boss.Attack_R.tag = "BossAttack";
                if (attackcnt)
                {
                    boss.boss_Eff.InstantiateEffect(1, boss.Attack_R, 2f);
                    attackcnt = false;
                }

            }
        }

    }
}
