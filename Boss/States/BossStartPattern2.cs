﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartPattern2 : IState<Boss>
{
    float speed;
    private Vector3 _target = Vector3.back;

    public void OperateEnter(Boss boss)
    {
        boss.curtime = 0;
        boss.statename = "Patten2";
        boss.animator = boss.GetComponent<Animator>();
        boss.animator.SetBool("Patten2", true);

        Vector3 _target = -Vector3.forward;
        speed = 5.0f;
    }

    public void OperateExit(Boss boss)
    {
        boss.animator.SetBool("Patten2", false);

    }

    public void OperateUpdate(Boss boss)
    {
        if (boss.curtime >= 1.7f)
        {
            // 초기화
            boss.curtime = 0;
            boss.ChangeState(Boss.BossState.attackRight);
        }
        else
        {
            // 동작할때 처리
            boss.transform.Translate(-Vector3.forward * speed * Time.deltaTime);
        }

    }
}
