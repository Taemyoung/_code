﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartTurn : IState<Boss>
{
    float speed = 2.0f;
    private Vector3 _target;

    public void OperateEnter(Boss boss)
    {
        boss.curtime = 0;
        boss.statename = "Turn";
        boss.animator = boss.GetComponent<Animator>();
        
        if (boss.player_LR > 0)
        {
            boss.animator.SetBool("TurnRight", true);
        }
        else
        {
            boss.animator.SetBool("TurnLeft", true);
        }
        // Player 위치 가지고오기
        _target = boss._target.position;
    }

    public void OperateExit(Boss boss)
    {
        boss.animator.SetBool("TurnRight", false);
        boss.animator.SetBool("TurnLeft", false);
    }

    public void OperateUpdate(Boss boss)
    {
        if (boss.curtime >= 1f)
        {
            // 초기화
            boss.curtime = 0;
            boss.ChangeState(Boss.BossState.jump);
        }
        else
        {
            // 동작할때 처리
            Vector3 dir = _target - boss.transform.position;
            boss.transform.rotation = Quaternion.Lerp(boss.transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * speed);


        }

    }


}
