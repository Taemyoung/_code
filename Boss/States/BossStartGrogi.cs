﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartGrogi : IState<Boss>
{
    float grogitime;
    bool attackcnt = true;
    bool effcnt = true;

    public void OperateEnter(Boss boss)
    {
        boss.curtime = 0;
        grogitime = 0;
        boss.statename = "Grogi";
        boss.animator.SetBool("walk", false);
        boss.animator.SetBool("AttackLeft", false);
        boss.animator.SetBool("AttackRight", false);
        boss.animator.SetBool("Grogi", false);
        boss.animator.SetBool("Die", false);
        boss.animator.SetBool("Jump", false);
        boss.animator.SetBool("Patten1", false);
        boss.animator.SetBool("TurnRight", false);
        boss.animator.SetBool("TurnLeft", false);
        boss.animator.SetTrigger("Grogi");
        attackcnt = true;
        effcnt = true;
    }
    public void OperateExit(Boss boss)
    {
        boss.animator.SetTrigger("Wake");
    }
    public void OperateUpdate(Boss boss)
    {
        grogitime += Time.deltaTime;

        if (grogitime >= 8f)
        {
            // hit 및 grogi 초기화 
            boss._hitcnt = 0;
            boss._grogicnt = 0;

            // 초기화
            boss.curtime = 0;
            boss.boss_Eff.InstantiateEffect(3, boss.Attack_G, 2f);
            boss.ChangeState(Boss.BossState.pattern1);
        }
        else
        {

            boss.curtime = 0;

            if (grogitime >= 7f)
            {
                if (effcnt)
                {
                    boss.boss_Eff.InstantiateEffect(2 , boss.Attack_G, 2f);
                    effcnt = false;
                }
                if (attackcnt)
                {
                    attackcnt = false;
                }

            } 

        }
    }


}
