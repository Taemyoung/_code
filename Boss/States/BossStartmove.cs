﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossStartmove : IState<Boss>
{
    private NavMeshAgent nvAgent;
    private Vector3 _target;
    
    public void OperateEnter(Boss boss)
    {
        boss.curtime = 0;
        // statename 상태
        boss.statename = "walk";

        nvAgent = boss.myobj.GetComponent<NavMeshAgent>();

        boss.animator.SetBool("walk", true); 
        _target = boss._target.position;

        nvAgent.SetDestination(_target);
        nvAgent.Resume();
    }

    public void OperateExit(Boss boss)
    {
        boss.animator.SetBool("walk", false);
    }

    public void OperateUpdate(Boss boss)
    {
        if (boss.curtime >= boss.delaytime)
        {
            // 초기화
            boss.curtime = 0;
            boss.ChangeState(Boss.BossState.idle);
        }
        else if (_target != null)
        {
            Vector3 offset = _target - boss._mytrans.position;
            float sprLen = offset.sqrMagnitude; // 플레이어 랑 보스랑 거리 구하기

            if (sprLen <= 40)
            {
                nvAgent.Stop();
                boss.ChangeState(Boss.BossState.idle);
            }
        }
    }

  
}
