﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossStartJump : IState<Boss>
{
    float speed = 4.0f;
    private Vector3 _target;

    public void OperateEnter(Boss boss)
    {
        boss.curtime = 0;
        boss.statename = "Jump";
        boss.animator = boss.GetComponent<Animator>();
        boss.animator.SetBool("Jump", true);
        
        // 타겟 위치 가지고오기
        _target = (boss._target.position + new Vector3(0,0, -10));
    }

    public void OperateExit(Boss boss)
    {
        boss.animator.SetBool("Jump", false);
    }

    public void OperateUpdate(Boss boss)
    {
        if (boss.curtime >= boss.delaytime)
        {
            // 초기화
            boss.curtime = 0;
            boss.ChangeState(Boss.BossState.idle);
        } else
        {
            Vector3 offset = _target - boss._mytrans.position;
            float sprLen = offset.sqrMagnitude; // 플레이어 랑 보스랑 거리 구하기

            Vector3 targetpos = (_target - boss._mytrans.position).normalized;
            boss.boss_rigi.velocity = targetpos * speed;

            if (sprLen <= 40)
            {
                 boss.ChangeState(Boss.BossState.idle);
            }
            
        }

    }

}
