﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;

public class TestHit : MonoBehaviour
{
    PlayerController Player;
    Material mat;
    Rigidbody rigidbody;
    Transform PlayerPos ;
    float UpTime;
    NavMeshAgent navmesh;

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Pushattack")
        {
            rigidbody.velocity = Vector3.zero;
            Debug.Log("Pushattack");

            Debug.Log("AttackHit");
            Vector3 target = PlayerPos.transform.position;

            Vector3 a = transform.position - target;
            a.y = 0.3f;
            a.x += 1f;
            a.z += 1f;

            // transform.DOMove(transform.position + a.normalized * 2 , 0.3f);

            rigidbody.AddForce(a * 3, ForceMode.Impulse); 

            StartCoroutine(OnDamage());
        }

        if (other.tag == "Attack")
        {
            rigidbody.velocity = Vector3.zero;
            Debug.Log("AttackHit");
            Vector3 target = PlayerPos.transform.position;

            Vector3 a = transform.position - target ;
            // a.y = 0;
            StartCoroutine(OnDamage());

            //  transform.DOMove(transform.position + a.normalized / 2 , 0.3f);

            rigidbody.AddForce(a * 1 , ForceMode.Impulse);
            // transform.position += a.normalized;
        }

        if (other.tag == "UpperAttack")
        {
            UpTime = 0;
            rigidbody.velocity = Vector3.zero;
            Debug.Log("UpperAttackHit");

            Debug.Log("AttackHit");
            Vector3 target = PlayerPos.transform.position;

            Vector3 a = transform.position - target;
            a.y = 6;
            a.z = 0;
            a.x = 0;
            // transform.DOMoveY(3f, 0.5f);

            rigidbody.AddForce(a * 2, ForceMode.Impulse);

            StartCoroutine(OnDamage());
        }
        if (other.tag == "UpperAttackIng")
        {

            UpTime = 0;
            rigidbody.velocity = Vector3.zero;
            Debug.Log("UpperAttackHit- ING");

            Debug.Log("AttackHit - ING");
            Vector3 target = PlayerPos.transform.position;

            Vector3 a = transform.position - target;
            a.y = 7;

            a.z = 0;
            a.x = 0;
            // transform.DOMoveY(transform.position.y, 0.5f);

            rigidbody.AddForce(a * 1, ForceMode.Impulse);
            StartCoroutine(OnDamage());

        }
    }

    IEnumerator OnDamage()
    {

        mat.color = Color.red;



        yield return new WaitForSeconds(0.1f);

        mat.color = Color.white;
    }

    private void OnTriggerExit(Collider other)
    {


    }

    void Start()
    {
        UpTime = 0;
    }
    private void Awake()
    {
        PlayerPos = GameObject.Find("Player").transform;
        Player = GameObject.FindObjectOfType<PlayerController>();
        mat = GetComponent<SkinnedMeshRenderer>().material;
        rigidbody = GetComponent<Rigidbody>();
        navmesh = GetComponent<NavMeshAgent>();

        navmesh.SetDestination(PlayerPos.position);
        navmesh.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        // transform.DOMove(new Vector3(transform.position.x, 0.8f, transform.position.z), 0.2f);

            UpTime += Time.deltaTime;

            if (UpTime >= 1f)
            {
                navmesh.enabled = false;
                UpTime = 0;
                // transform.DOMoveY(0.5f, 0.3f);
            }
    }
}
