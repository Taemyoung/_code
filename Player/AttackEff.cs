﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackEff : MonoBehaviour
{
    Vector3 originPos;
    public CameraMove CameraEff;

    // Start is called before the first frame update
    void Start()
    {
        originPos = transform.localPosition;

    }
    private void Awake()
    {
        CameraEff = GameObject.Find("Main Camera").GetComponent<CameraMove>();
    }

    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enamy")
        {
            StartCoroutine(HitMosion());
        }
    }

    IEnumerator HitMosion()
    {
        Debug.LogError("HIt");
        yield return new WaitForSecondsRealtime(0.2f);

        // CameraEff.Shake(0.3f, 0.4f);

    }
    public void SetTimeScale(float time)
    {
        Time.timeScale = time;
        Time.fixedDeltaTime = 0.02f * time;
    }

    public IEnumerator Shake(float _amount, float _duration)
    {
        float timer = 0;
        while (timer <= _duration)
        {
            transform.localPosition = (Vector3)Random.insideUnitCircle * _amount + originPos;

            timer += Time.deltaTime;
            yield return null;
        }
        transform.localPosition = originPos;

    }

}
