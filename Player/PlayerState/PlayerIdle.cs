﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdle : IState<PlayerController>
{
    float speed;
    float RunTime_Speed;
    float rot_Speed;
    bool grogi;
    public void OperateEnter(PlayerController Player)
    {
        RunTime_Speed = 0.5f;
        speed = 4.0f;
        rot_Speed = 4.0f; 
        Player.transform.DOMoveY(0f, 0.1f);
        Player.BodyEff.GetComponent<TrailsFX.TrailEffect>().active = false;
        Player.WeaponEff.SetActive(false);

    }

    public void OperateExit(PlayerController Player)
    {
        Player.transform.position = Player.transform.position;
        Player.transform.rotation = Player.transform.rotation;
        Player.animator.SetBool("Ismove", false);
        Player.animator.SetFloat("horizontal", 0f);
        Player.animator.SetFloat("vertical", 0f);

    }

    public void OperateUpdate(PlayerController Player)
    {
        bool LeftShift = Input.GetKey(KeyCode.LeftShift);
        if (Player.Runtime <= 0f)
        {
            grogi = false;
        }
        else if(Player.Runtime > 1f && LeftShift == true)
        {
            grogi = true;
        }

        KeyInput_Move(Player);

        if (Input.GetMouseButtonDown(0) && Player.animator.GetFloat("vertical") == 1.0f)
        {
            Player.ChangeState(PlayerController.PlayerState.DashAattack);
        }
        else if (Input.GetMouseButtonDown(0))
        {
            Player.animator.SetBool("IsAttack", true);
            Player.ChangeState(PlayerController.PlayerState.attack);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Player.ChangeState(PlayerController.PlayerState.Revenge);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (Player.CoolTimeUi.GetComponent<CoolTimeUi>().currentCooldown < 0f)
            {
                Player.ChangeState(PlayerController.PlayerState.UpperAttack);
            }
            else
            {
                Player._CoolTimeText("CoolTime_Skill_R");
            }
        }

    }


    Vector3 KeyInput_Move(PlayerController Player)
    {
        Vector3 moveMent;
        bool LeftShift = Input.GetKey(KeyCode.LeftShift);
        if (LeftShift == false)
        {
            Player.Runtime += Time.deltaTime * RunTime_Speed;

            float h = Input.GetAxis("Horizontal") / 2;
            Player.animator.SetFloat("horizontal", h);
            float v = Input.GetAxis("Vertical") / 2;
            Player.animator.SetFloat("vertical", v);
            moveMent = new Vector3(h, 0f, v).normalized;
            Player.transform.Translate(moveMent * Time.deltaTime * (speed) );
            if (moveMent != Vector3.zero)
            {
                Player.animator.SetBool("Ismove", true);
            }
            else
            {
                Player.animator.SetBool("Ismove", false);
            }
        }
        else 
        {

            Player.Runtime -= Time.deltaTime * RunTime_Speed;

            float h = Input.GetAxis("Horizontal") / 2;
            Player.animator.SetFloat("horizontal", h);
            float v = Input.GetAxis("Vertical");
            Player.animator.SetFloat("vertical", v);
            moveMent = new Vector3(h, 0f, v * 2).normalized;
            if (moveMent != Vector3.zero)
            {
                Player.animator.SetBool("Ismove", true);
            }
            else
            {
                Player.animator.SetBool("Ismove", false);
            }
        }

        float x = Input.GetAxis("Horizontal") / 2;
        float MouseX = Input.GetAxis("Mouse X");
        Player.transform.Rotate(Vector3.up * rot_Speed * MouseX );

        return moveMent;
    }


}
