﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : IState<PlayerController>
{


    public void OperateEnter(PlayerController Player)
    {

        Player.BodyEff.GetComponent<TrailsFX.TrailEffect>().active = true;
        Player.WeaponEff.SetActive(true);

        Player.Attack_H.Clear();
        Player.StartCoroutine(LeftAttack1(Player));
    }

    public void OperateExit(PlayerController Player)
    {
        Player.animator.SetBool("LeftAttack0", false);
        Player.animator.SetBool("LeftAttack1", false);
        Player.transform.DOMoveY(0, 0.2f);
        Player.FalseHitBox();

        Player.BodyEff.GetComponent<TrailsFX.TrailEffect>().active = false;
        Player.WeaponEff.SetActive(false);
    }

    public void OperateUpdate(PlayerController Player)
    {

    }

    IEnumerator LeftAttack1(PlayerController Player)
    {
        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Attack_3Combo_1"))
        {
            yield return null;
        }
        // Hit on
        Player.OnHitBox(2);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.5f)
        {

            bool Leftattack = Input.GetMouseButtonDown(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }
        // Hit oFF
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.Attack_H.Clear();
                Player.animator.SetTrigger("LeftAttack0");
                Player.StartCoroutine(LeftAttack2(Player));
            }
            else if(triggerMouse == 2)
            {
                // 1222 패턴 제작 새로운 스크립트 만들기 
                Player.Attack_H.Clear();
                Player.animator.SetTrigger("RightAttack");
                Player.ChangeState(PlayerController.PlayerState.Rightattack1);
            }

        }
        else
        {

            Player.animator.SetBool("IsAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }

    }

    IEnumerator LeftAttack2(PlayerController Player)
    {
        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Attack_3Combo_2"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(2);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.7f)
        {

            bool Leftattack = Input.GetMouseButtonDown(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        // Hit oFF
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.Attack_H.Clear();
                Player.animator.SetTrigger("LeftAttack1");
                Player.StartCoroutine(LeftAttack3(Player));
            }
            else if (triggerMouse == 2)
            {
                //  11222 패턴제작 스트립트 만들기
                Player.Attack_H.Clear();
                Player.animator.SetTrigger("RightAttack");
                Player.ChangeState(PlayerController.PlayerState.Rightattack2);
            }

        }
        else
        {

            Player.animator.SetBool("IsAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }

    }

    IEnumerator LeftAttack3(PlayerController Player)
    {
        Player.transform.position = new Vector3(Player.transform.position.x, 0f, Player.transform.position.z);
        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Attack_3Combo_3_short distance_Return"))
        {
            yield return null;
        }
        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.3f)
        {

            yield return null;
        }

        // Hit on
        Player.OnHitBox(5);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.7f)
        {

            yield return null;
        }

        // Hit oFF
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        Player.animator.SetBool("IsAttack", false);
        Player.ChangeState(PlayerController.PlayerState.idle);
        Debug.Log("LeftAttack 111 End");
        Player.Attack_H.Clear();
        

    }


}
