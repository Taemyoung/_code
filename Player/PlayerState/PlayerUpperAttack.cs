﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerUpperAttack : IState<PlayerController>
{
    float time;

    public void OperateEnter(PlayerController Player)
    {
        Player.CoolTimeUi.GetComponent<CoolTimeUi>().CoolTimeStart();
        Player.BodyEff.GetComponent<TrailsFX.TrailEffect>().active = true;
        Player.WeaponEff.SetActive(true);

        Player.Attack_H.Clear();
        Player.animator.SetBool("IsUpperAttack", true);
        Player.StartCoroutine(UpperAttack2(Player));
        // Player.transform.DOMoveY(2, 0.2f);
        Player.FalseHitBox();

    }

    public void OperateExit(PlayerController Player)
    {

        Player.BodyEff.GetComponent<TrailsFX.TrailEffect>().active = false;
        Player.WeaponEff.SetActive(false);

        Player.animator.SetBool("IsUpperAttack", false);
        Player.transform.DOMoveY(0, 0.2f);

    }

    public void OperateUpdate(PlayerController Player)
    {
        time += Time.deltaTime;

        if (time > 0.5f)
        {
           
            // Player.transform.DOMoveY(2.0f,0.2f);
            time = 0f;
        }


    }

    IEnumerator UpperAttack2(PlayerController Player)
    {

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("UpperAttack"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(4);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.4f)
        {
            bool Leftattack = Input.GetMouseButtonDown(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.StartCoroutine(UpperAttack3(Player));
                Player.Attack_H.Clear();
            }
            else if (triggerMouse == 2)
            {
                Player.Attack_H.Clear();
                Player.animator.SetBool("IsUpperAttack", false);
                Player.ChangeState(PlayerController.PlayerState.idle);
                Debug.Log("Nocount");
            }

        }
        else
        {
            while (Player.animator.GetCurrentAnimatorStateInfo(0)
            .normalizedTime <= 0.9f)
            {
                yield return null;
            }

            Player.animator.SetBool("IsUpperAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }
    }

    IEnumerator UpperAttack3(PlayerController Player)
    {

        Player.animator.SetTrigger("LeftAttack0");

        // position 처리  
        // Player.transform.position = new Vector3(Player.transform.position.x, 2.2f, Player.transform.position.z);

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Jump_Attack_Combo_1"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(6);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.4f)
        {
            bool Leftattack = Input.GetMouseButton(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        while (Player.animator.GetCurrentAnimatorStateInfo(0)
       .normalizedTime < 0.8f)
        {
            yield return null;
        }

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.StartCoroutine(UpperAttack4(Player));
                Player.Attack_H.Clear();
            }
            else if (triggerMouse == 2)
            {
                Player.Attack_H.Clear();
                Player.animator.SetBool("IsUpperAttack", false);
                Player.ChangeState(PlayerController.PlayerState.idle);
                Debug.Log("Nocount");
            }

        }
        else
        {
            Player.animator.SetBool("IsUpperAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }
    }

    IEnumerator UpperAttack4(PlayerController Player)
    {

        Player.animator.SetTrigger("LeftAttack0");

        // position 처리  
        // Player.transform.position = new Vector3(Player.transform.position.x, 2.2f, Player.transform.position.z);

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Jump_Attack_Combo_2_Attach"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(6);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.6f)
        {
            bool Leftattack = Input.GetMouseButton(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.StartCoroutine(UpperAttack5(Player));
                Player.Attack_H.Clear();
            }
            else if (triggerMouse == 2)
            {
                Player.Attack_H.Clear();
                Player.animator.SetBool("IsUpperAttack", false);
                Player.ChangeState(PlayerController.PlayerState.idle);
                Debug.Log("Nocount");
            }

        }
        else
        {
            Player.animator.SetBool("IsUpperAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }
    }

    IEnumerator UpperAttack5(PlayerController Player)
    {

        Player.animator.SetTrigger("LeftAttack0");

        // position 처리  
        // Player.transform.position = new Vector3(Player.transform.position.x, 2.2f, Player.transform.position.z);

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Jump_Attack_Combo_3_Attach"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(6);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.6f)
        {
            bool Leftattack = Input.GetMouseButton(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.StartCoroutine(UpperAttack6(Player));
                Player.Attack_H.Clear();
            }
            else if (triggerMouse == 2)
            {
                Player.Attack_H.Clear();
                Player.animator.SetBool("IsUpperAttack", false);
                Player.ChangeState(PlayerController.PlayerState.idle);
                Debug.Log("Nocount");
            }

        }
        else
        {
            Player.animator.SetBool("IsUpperAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }
    }

    IEnumerator UpperAttack6(PlayerController Player)
    {

        Player.animator.SetTrigger("LeftAttack0");

        // position 처리  
        // Player.transform.position = new Vector3(Player.transform.position.x, 2.2f, Player.transform.position.z);

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Jump_Attack_Combo_4_Attach"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(6);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.6f)
        {
            bool Leftattack = Input.GetMouseButton(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.StartCoroutine(UpperAttack7(Player));
                Player.Attack_H.Clear();
            }
            else if (triggerMouse == 2)
            {
                Player.Attack_H.Clear();
                Player.animator.SetBool("IsUpperAttack", false);
                Player.ChangeState(PlayerController.PlayerState.idle);
                Debug.Log("Nocount");
            }

        }
        else
        {
            Player.animator.SetBool("IsUpperAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }
    }

    IEnumerator UpperAttack7(PlayerController Player)
    {

        Player.animator.SetTrigger("LeftAttack0");

        // position 처리  
        // Player.transform.position = new Vector3(Player.transform.position.x, 2.2f, Player.transform.position.z);

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Jump_Attack_Combo_5_Attach"))
        {
            yield return null;
        }


        // Hit on
        Player.OnHitBox(6);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.6f)
        {
            bool Leftattack = Input.GetMouseButton(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.StartCoroutine(UpperAttackEnd(Player));
                Player.Attack_H.Clear();
            }
            else if (triggerMouse == 2)
            {
                Player.Attack_H.Clear();
                Player.animator.SetBool("IsUpperAttack", false);
                Player.ChangeState(PlayerController.PlayerState.idle);
                Debug.Log("Nocount");
            }

        }
        else
        {
            Player.animator.SetBool("IsUpperAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }
    }

    IEnumerator UpperAttackEnd(PlayerController Player)
    {
        Player.animator.SetTrigger("LeftAttack0");

        // position 처리  
        // Player.transform.position = new Vector3(Player.transform.position.x, 2.2f, Player.transform.position.z);

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Jump_Attack_Combo_6_Attach"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(5);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.7f)
        {
            bool Leftattack = Input.GetMouseButton(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime <= 0.9f)
        {
            yield return null;
        }

        // Player.transform.position = new Vector3(Player.transform.position.x, 0f, Player.transform.position.z);
        Player.animator.SetBool("IsUpperAttack", false);
        Player.ChangeState(PlayerController.PlayerState.idle);
        Debug.Log("Nocount");
        Player.Attack_H.Clear();

    }
}
