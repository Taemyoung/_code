﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDashAttack : IState<PlayerController>
{


    public void OperateEnter(PlayerController Player)
    {

        Player.BodyEff.GetComponent<TrailsFX.TrailEffect>().active = true;
        Player.WeaponEff.SetActive(true);

        Player.animator.SetTrigger("IsDashAttack");

        Player.StartCoroutine(DashAttack2(Player));

    }

    public void OperateExit(PlayerController Player)
    {

        Player.BodyEff.GetComponent<TrailsFX.TrailEffect>().active = false;
        Player.WeaponEff.SetActive(false);

        Player.FalseHitBox();

    }

    public void OperateUpdate(PlayerController Player)
    {

    }


    IEnumerator DashAttack2(PlayerController Player)
    {
        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Dash_Attack_ver_A"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(2);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.7f)
        {
            bool Rightattack = Input.GetMouseButtonDown(1);

           
            if (Rightattack)
            {
                Player.Attack_H.Enqueue(2);
            }

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            if (triggerMouse == 2)
            {
                Player.Attack_H.Clear();
                Player.animator.SetTrigger("LeftAttack0");
                Player.StartCoroutine(DashAttackEnd(Player));

            }

        }
        else
        {

            Player.animator.SetBool("IsAttack", false);
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("Nocount");
            Player.Attack_H.Clear();
        }

    }

    IEnumerator DashAttackEnd(PlayerController Player)
    {
        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Dash_Attack_ver_B"))
        {
            yield return null;
        }

        // Hit on
        Player.OnHitBox(5);

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.8f)
        {

            yield return null;
        }

        //Hit off
        Player.FalseHitBox();

        //애니메이션 완료 후 실행되는 부분
        Player.transform.position = new Vector3(Player.transform.position.x, 0f, Player.transform.position.z);
        Player.animator.SetBool("IsAttack", false);
        Player.ChangeState(PlayerController.PlayerState.idle);
        Debug.Log("DashAttack End");
        Player.Attack_H.Clear();


    }

}
