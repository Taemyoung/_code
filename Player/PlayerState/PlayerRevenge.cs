﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRevenge : IState<PlayerController>
{

    public void OperateEnter(PlayerController Player)
    {
        Player.Attack_H.Clear();
        Player.animator.SetBool("IsRevenge", true);
        Player.StartCoroutine(RevengeStart(Player));
        
    }

    public void OperateExit(PlayerController Player)
    {
        Player.animator.SetBool("IsRevenge", false);

    }

    public void OperateUpdate(PlayerController Player)
    {

    }

    IEnumerator RevengeStart(PlayerController Player)
    {

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Revenge_Guard_Loop"))
        {
            yield return null;
        }

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 2.0f)
        {
            bool Leftattack = Input.GetMouseButtonDown(0);
            bool Rightattack = Input.GetMouseButtonDown(1);

            if (Leftattack)
            {
                Player.Attack_H.Enqueue(1);
            }

            yield return null;
        }

        //애니메이션 완료 후 실행되는 부분
        if (Player.Attack_H.Count > 0)
        {
            var triggerMouse = Player.Attack_H.Dequeue();
            Debug.Log(triggerMouse);

            // 다음 분기 처리
            if (triggerMouse == 1)
            {
                Player.Attack_H.Clear();
                Player.animator.SetTrigger("RevengeHit");
                Player.StartCoroutine(RevengeEnd(Player));
            }
        }
        else
        {

            Player.animator.SetTrigger("RevengeNoHit");
            Player.ChangeState(PlayerController.PlayerState.idle);
            Debug.Log("NoHit");
            Player.Attack_H.Clear();
        }

    }

    IEnumerator RevengeEnd(PlayerController Player)
    {

        //전환 중일 때 실행되는 부분
        while (!Player.animator.GetCurrentAnimatorStateInfo(0)
        .IsName("Revenge_Guard_Attack_ver_B"))
        {
            yield return null;
        }

        //애니메이션 재생 중 실행되는 부분
        while (Player.animator.GetCurrentAnimatorStateInfo(0)
        .normalizedTime < 0.8f)
        {
            yield return null;
        }

         Player.ChangeState(PlayerController.PlayerState.idle);
         Debug.Log("Attack end");
         Player.Attack_H.Clear();
       

    }
}
