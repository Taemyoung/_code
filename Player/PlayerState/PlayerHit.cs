﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHit : IState<PlayerController>
{
    float HitTime;
    public void OperateEnter(PlayerController Player)
    {
        HitTime = 0;
        
        Player.animator.SetTrigger("IsHit");

    }

    public void OperateExit(PlayerController Player)
    {

    }

    public void OperateUpdate(PlayerController Player)
    {
        HitTime += Time.deltaTime;

        if(HitTime > 0.5f)
        {
            Player.ChangeState(PlayerController.PlayerState.idle);
        }
        
    }

}
