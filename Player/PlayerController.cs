﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;


public class PlayerController : MonoBehaviour
{
    public int PlayerDag;
    public float Runtime;

    int MaxHp;
    public float CurHp;
    public GameObject cursor;
    public BoxCollider HitPoint;
    public Queue<int> Attack_H = new Queue<int>();

    public float Max_Health;
    public float Cur_Health;
    public float Damage;

    public string Cur_Abnormal;

    public Vector3 targetpos;

    public Transform PlayerTrans;

    public float speed = 3.0f;
    public Animator animator;
    public Rigidbody rigidbody;
    public CapsuleCollider capsulecollider;

    public GameObject CoolTimeText;
    public GameObject CoolTimeUi;
    public int CoolTime_R;

    public GameObject WeaponEff;
    public GameObject BodyEff;

    public enum PlayerState
    {
        idle,
        attack,
        Rightattack1,
        Rightattack2,
        DashAattack,
        UpperAttack,
        Revenge,
        hit,

    }
    

    private Set_State<PlayerController> stateMachine;
    private Dictionary<PlayerState, IState<PlayerController>> dicstate = new Dictionary<PlayerState, IState<PlayerController>>();

    void Start()
    {
        CoolTime_R = 6;
        BodyEff.GetComponent<TrailsFX.TrailEffect>().active = false;

        MaxHp = 10;
        CurHp = MaxHp;
        Cur_Abnormal = "normal";
        Max_Health = 100f;
        Cur_Health = Max_Health;
        Damage = 50f;
        speed = 3f;

        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody>();
        capsulecollider = GetComponent<CapsuleCollider>();


        // 상태 생성
        IState<PlayerController> idle = new PlayerIdle();
        IState<PlayerController> attack = new PlayerAttack();
        IState<PlayerController> Rightattack1 = new PlayerRightAttack1();
        IState<PlayerController> Rightattack2 = new PlayerRightAttack2();
        IState<PlayerController> DashAattack = new PlayerDashAttack();
        IState<PlayerController> UpperAttack = new PlayerUpperAttack();
        IState<PlayerController> Revenge = new PlayerRevenge();
        IState<PlayerController> hit = new PlayerHit();
     
        // 상태 보관
        dicstate.Add(PlayerState.idle, idle);
        dicstate.Add(PlayerState.attack, attack);
        dicstate.Add(PlayerState.Rightattack1, Rightattack1);
        dicstate.Add(PlayerState.Rightattack2, Rightattack2);
        dicstate.Add(PlayerState.DashAattack, DashAattack);
        dicstate.Add(PlayerState.UpperAttack, UpperAttack);
        dicstate.Add(PlayerState.Revenge, Revenge);
        dicstate.Add(PlayerState.hit, hit);
     

        stateMachine = new Set_State<PlayerController>(this, dicstate[PlayerState.idle]);
    }
    private void Awake()
    {
        HitPoint = GameObject.Find("HitPoint").GetComponent<BoxCollider>();
        HitPoint.enabled = false; 
        Cursor.visible = false;


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "EnamyAttack")
        {
            rigidbody.velocity = Vector3.zero;
            Vector3 target = other.transform.position;

            Vector3 a = transform.position - target;
            a.y = 0.3f;
            a.x += 1f;
            a.z += 1f;
            CurHp -= 0.5f;
        }

    }
    public void OnHitBox(int a)
    {
        switch (a)
        {

            case 1:
                HitPoint.tag = "idle";
                break;
            case 2:
                HitPoint.tag = "Attack";
                break;
            case 3:
                HitPoint.tag = "DownAttack";
                break;
            case 4:
                HitPoint.tag = "UpperAttack";
                break;
            case 5:
                HitPoint.tag = "Pushattack";
                break;
            case 6:
                HitPoint.tag = "UpperAttackIng";
                break;

            default:
                break;

        }

        HitPoint.enabled = true;
    }

    public void FalseHitBox()
    {
        HitPoint.tag = "idle";
        HitPoint.enabled = false;
    }

    public void ChangeState(PlayerState state)
    {
        stateMachine.SetState(dicstate[state]);
    }

    void Update()
    {
        if (Runtime > 1f)
        {
            Runtime = 1;
        }
        stateMachine.DoOperateUpdate();

        PlayerDag = Random.Range(1, 6);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Camera.main.DOShakeRotation(0.5f, 3,fadeOut: true);
        }

    }

    public void _CoolTimeText(string CoolText)
    {
        GameObject hudText = Instantiate(CoolTimeText); 
        hudText.transform.SetParent(GameObject.Find("Canvas").transform);
        hudText.transform.position = new Vector3(550, 50, 0);
        hudText.GetComponent<CoolTimeText>().CoolTime_Text = CoolText; 

    }
    public void BackIdle()
    {
        ChangeState(PlayerState.idle);
    }

}
