﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    CharacterController Player;
    Animator animator;
    float rot_Speed;

    // Start is called before the first frame update
    void Start()
    {
        rot_Speed = 3f;
        Player = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {

    }
    private void FixedUpdate()
    {
        KeyInput_Move();

    }

    Vector3 KeyInput_Move()
    {
        Vector3 moveMent;
        bool LeftShift = Input.GetKey(KeyCode.LeftShift);
        if (LeftShift == false)
        {
            float h = Input.GetAxis("Horizontal") / 2;
            animator.SetFloat("horizontal", h);
            float v = Input.GetAxis("Vertical") / 2;
            animator.SetFloat("vertical", v);
            moveMent = new Vector3(h, 0f, v).normalized;
            if (moveMent != Vector3.zero)
            {
                animator.SetBool("Ismove", true);
            }
            else
            {
                animator.SetBool("Ismove", false);
            }
        }
        else
        {
            float h = Input.GetAxis("Horizontal") / 2;
            animator.SetFloat("horizontal", h);
            float v = Input.GetAxis("Vertical");
            animator.SetFloat("vertical", v);
            moveMent = new Vector3(h, 0f, v).normalized;
            if (moveMent != Vector3.zero)
            {
                animator.SetBool("Ismove", true);
            }
            else
            {
                animator.SetBool("Ismove", false);
            }
        }

        float MouseX = Input.GetAxis("Mouse X");



        transform.Rotate(Vector3.up * rot_Speed * MouseX);

        return moveMent;
    }
   


}
